import React, {Component} from 'react'

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completeLine ? "completed" : ""}>
        <div className="view">
          <input className="toggle" 
          type="checkbox" 
          checked={this.props.completeLine}
          onChange={this.props.completeToggle} />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleDeleteList}/>
        </div>
      </li>
    );
  }
}
  export default TodoItem