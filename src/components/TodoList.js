import React, {Component} from 'react'
import TodoItem from './TodoItem'


class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem title={todo.title}
            key={todo.id} 
            completed={todo.completedLine} 
            handleDeleteList={this.props.handleDeleteList(todo.id)}
            completedLine={todo.completedLine}
            completedTrigger={this.props.completeTrigger(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}
export default TodoList