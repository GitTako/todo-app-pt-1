import React, { Component } from "react";
import todosList from './todos.json'
import TodoList from './components/TodoList'
class App extends Component {
  state = {
    todos: todosList,
    value:''
  };

  handleChance = event => {
    this.setState({value: event.target.value})
  }

  handleCreate = event => {
    if (event.key === 'Enter'){
      let newTodo ={
        userId: 1,
        id:Math.floor(Math.random()*10000000),
        title: this.state.value,
        completed: false
      }
     const newTodos = this.state.todos.slice()
      newTodos.push(newTodo)
      this.setState({todos: newTodos, value: ''})
    } 
  }
  handleDeleteList = todoIdDelete => (event) => {
    const newTodos = this.state.todos.slice()
    const todoIndex = newTodos.findIndex(todos => {
      if (todos.id === todoIdDelete){
        return true
      } else {
        return false
      }
    })
    newTodos.splice(todoIndex, 1)
    this.setState({todos: newTodos})
  }

  handleDeleteSel = () => {
    const newTodos = this.state.todos.filter(todo => {
      if (todo.completed === true){
        return false
      }else{
        return true
      }
    })
    this.setState({todos: newTodos})
  }

  completeTrigger = (todoIdTrigger) => (event) =>{
    const newTodo = this.state.todos.map(todo => {
      if(todo.id === todoIdTrigger){
        if(todo.completed === true){
          return todo.completed = false;
        } else {
          todo.completed = true;
        } }return todo
    })
    this.setState({todos: newTodo})
  }

  completedLine = (todos) => {
    if(todos.completed === true){
      return todos.completed = true
    } else{
      return todos.completed = false
    }
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" 
          placeholder="What needs to be done?" 
          autoFocus
          value={this.state.value}
          onChange={this.handleChance}
          onKeyDown={this.handleCreate} />
        </header>
        <TodoList
        handleDeleteList={this.handleDeleteList}
         todos={this.state.todos}
         completeTrigger={this.completeTrigger} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" 
          onClick={this.handleDeleteSel}>Clear completed</button>
        </footer>
      </section>
    );
  }
}


export default App;
